#!/usr/bin/env python3.9
# -*- coding: utf-8 -*- 
# python3.9 highest.py --filedata example_input_data_1.data  --numoutput 5
# gitlab.com/0x123451/eclhighrank
import os,sys,re,ast,json,argparse
import pandas as pd
from datetime import datetime


class HighestScoresProcessing():
    """ usage  : arguments --filedata ~/dah.data --numoutput 42 
        output to stdout in json             < score : id > 
    """
    def __init__(self,encoding="utf-8"):
        parser = argparse.ArgumentParser() 
        parser.add_argument('--filedata','-f', nargs='+',help='path to single file ')
        parser.add_argument('--numoutput','-r',nargs='+',help='number of output records')
        args = parser.parse_args()
        self.fileData = args.filedata[0]; self.numOutput = int(args.numoutput[0])
        self._starttimestamp(); self._dataparser()
        
    def _starttimestamp(self):
        """ log init or smth """
        print (' >>> ECL highScore data processing started ... ', str(datetime.now()))
        print('file to proccess: ', self.fileData); print('number of lines to output : ',self.numOutput)

    def _regexp_to_unsorted_dict(self,str):
        """ return unsorted dict """
        scoreRes = re.compile(r'[0-9]+:+.?')
        return {"score": int(re.search(scoreRes,str).group(0)[:-2]) , "id": ast.literal_eval(str.split(re.search(scoreRes,str).group(0))[1])['id']}

    def _dataparser(self):
        """ main loop for data parsing with regexp, ast, pandas, os, sys """
        if not os.path.isfile(self.fileData):
           print("File path {} does not exist. Exiting...".format(self.fileData))
           sys.exit()
        resList=[] # to feed data frame for pandas 
        
        with open(self.fileData) as fp:
            for line in fp:
                resList.append(self._regexp_to_unsorted_dict(line))
        
        rnkRes = pd.DataFrame.from_dict(resList)
        print ('-------- data frame sorted results ----------')
        self.rd = rnkRes.sort_values(by=['score'],ascending=False)
        print (self.rd)
        
        finalRes = self.rd.to_json(orient="records")
        parsedResToOutput = json.loads(finalRes) # int in slice list !
        outputjson = json.dumps(parsedResToOutput[:int(self.numOutput)], indent=4)
        
        print ('sorted JSON < score : id >')
        print (outputjson)        
        fp.close()

if __name__ == '__main__':
    ECL_test_python39_macosx = HighestScoresProcessing()
